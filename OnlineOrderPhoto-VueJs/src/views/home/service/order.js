import request from '@/utils/request'

export function getSize() {
  return request({
    url: `https://localhost:7107/api/Size/get-size`,
    method: 'get'
  })
}
