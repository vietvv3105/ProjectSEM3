import request from '@/utils/request'

export function getSize(data) {
  return request({
    url: `https://localhost:7107/api/Size/post-size`,
    method: 'get',
    data
  })
}
