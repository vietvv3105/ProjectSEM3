import paginationCommon from './pagination-common'
import loadingScreenCommon from './loading-screen-common'
import notificationCommon from './notification-common'
import dateTimeCommon from './date-time-common'
import authenCommon from './authen-common'
export default {
  mixins: [paginationCommon, loadingScreenCommon, notificationCommon, dateTimeCommon, authenCommon],
  methods: {
    dowloadFile(response, fileName) {
      const blob = new Blob([response])
      const url = window.URL.createObjectURL(blob)
      const link = document.createElement('a')
      link.href = url
      link.setAttribute('download', fileName)
      document.body.appendChild(link)
      link.click()
    }
  }
}

