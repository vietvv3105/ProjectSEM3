export default {
  methods: {
    hasPermission(componentName, action) {
      return this.$store.getters.actions[componentName?.toLowerCase()].indexOf(action) !== -1
    }
  }
}

