﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using WebApplication1.Data;
using WebApplication1.Dto;
using WebApplication1.Model;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class SizeController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private APIResponse _response;
        public SizeController(ApplicationDbContext db)
        {
            _db = db;
            _response = new APIResponse();
        }

        [HttpGet("get-size")]
        [Authorize]
        public async Task<ActionResult> Get()
        {
            var sizeData = _db.Sizes.ToList();
            return Ok(sizeData);
        }

        [HttpPost("post-size")]
        [Authorize]
        public async Task<ActionResult> Post([FromBody] SizeRequestDTO sizeCreateDTO)
        {

            SizeImage sizeImage = _db.Sizes.FirstOrDefault(s => s.PicSize == sizeCreateDTO.PicSize);

            if (sizeImage != null)
            {
                return BadRequest("Đã có dữ liệu picSize");
            }

            if (sizeCreateDTO.PicSize == null || sizeCreateDTO.SizePrice == 0) {
                return BadRequest("Nhập thiếu dữ liệu");
            }

            SizeImage img = new SizeImage()
            {
                PicSize = sizeCreateDTO.PicSize,
                SizePrice = sizeCreateDTO.SizePrice,
                CreateDate= DateTime.Now,
                UpdateAt= DateTime.Now,
            };

            await _db.Sizes.AddAsync(img);
            await _db.SaveChangesAsync();

            return Ok(img);
        }

        [HttpPut("put-size/{id}")]
        [Authorize]
        public async Task<ActionResult> Put([FromBody] SizeRequestDTO sizeRequestDTO, long id)
        {

            SizeImage sizeImage = _db.Sizes.FirstOrDefault(s => s.SizeId == id);
            SizeImage picSize = _db.Sizes.FirstOrDefault(p => p.PicSize == sizeRequestDTO.PicSize);

            if (sizeImage == null)
            {
                return BadRequest("Không có dữ liệu picSize trên DB");
            }

            if (sizeRequestDTO.PicSize == null || sizeRequestDTO.SizePrice == 0)
            {
                return BadRequest("Nhập thiếu dữ liệu");
            }

            if (picSize != null)
            {
                return BadRequest("Đã có dữ liệu trong DB");
            }

            sizeImage.PicSize = sizeRequestDTO.PicSize;
            sizeImage.SizePrice = sizeRequestDTO.SizePrice;
            sizeImage.UpdateAt = DateTime.Now;

            _db.Sizes.Update(sizeImage);
            await _db.SaveChangesAsync();

            return Ok(sizeImage);
        }

        [HttpDelete("delete-size/{id}")]
        [Authorize]
        public async Task<ActionResult> Delete(long id)
        {

            SizeImage sizeImage = _db.Sizes.FirstOrDefault(s => s.SizeId == id);

            if (sizeImage == null)
            {
                return BadRequest("Không có dữ liệu picSize trên DB");
            }

            _db.Sizes.Remove(sizeImage);
            await _db.SaveChangesAsync();

            return Ok($"Xóa size với id = {id} thành công");
        }
    }
}
