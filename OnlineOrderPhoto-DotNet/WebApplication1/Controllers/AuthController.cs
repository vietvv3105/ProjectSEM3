﻿using Azure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Security.Claims;
using System.Text;
using WebApplication1.Data;
using WebApplication1.Dto;
using WebApplication1.Dto.User;
using WebApplication1.Model;
using WebApplication1.Ultility;

namespace WebApplication1.Controllers
{
    [Route("/api/[controller]")]

    public class AuthController :ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private APIResponse _response;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly RoleManager<IdentityRole> _roleManager;
        private string secretKey;
        public AuthController(ApplicationDbContext db, IConfiguration configuration, RoleManager<IdentityRole> roleManager, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            secretKey = configuration.GetValue<string>("ApiSettings:SecretKey");
            _response = new APIResponse();
            _userManager = userManager;
            _roleManager = roleManager;

        }
        [HttpPost("login")]
        public async Task<ActionResult> Login([FromBody] LoginRequestDto model)
        {
            ApplicationUser userFromDb = _db.ApplicationUsers
                    .FirstOrDefault(u => u.Email.ToLower() == model.Email.ToLower());

            bool isValid = await _userManager.CheckPasswordAsync(userFromDb, model.Password);
            if (isValid == false)
            {
                _response.Data = new LoginRequestDto();
                _response.StatusCode = HttpStatusCode.UnprocessableEntity;
                _response.IsSuccess = false;
                _response.ErrorMessages = "Username or password is incorrect";
                return UnprocessableEntity(_response);
            }
            var roles = await _userManager.GetRolesAsync(userFromDb);
            JwtSecurityTokenHandler tokenHandler = new();
            byte[] key = Encoding.ASCII.GetBytes(secretKey);

            SecurityTokenDescriptor tokenDescriptor = new()
            {
                Subject = new ClaimsIdentity(new Claim[]
                {
                    new Claim("fullName", userFromDb.Email),
                    new Claim("id", userFromDb.Id.ToString()),
                    new Claim(ClaimTypes.Email, userFromDb.Email.ToString()),
                    new Claim(ClaimTypes.Role, roles.FirstOrDefault()),
                }),
                Expires = DateTime.UtcNow.AddDays(7),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
            };
            SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
            UserDto userDto = new UserDto()
            {
                Id = userFromDb.Id,
                PhoneNumber = userFromDb.PhoneNumber,
                Address = userFromDb.AddressUser,
                Dob = userFromDb.BirthDay,
                Gender = userFromDb.Gender,
                Name = userFromDb.Fulllname,
                Email = userFromDb.Email,

            };
            LoginResponseDto loginResponse = new()
            {
                userDto = userDto,
                AccessToken = tokenHandler.WriteToken(token)
            };

            if (userFromDb.Email == null || string.IsNullOrEmpty(loginResponse.AccessToken))
            {
                _response.StatusCode = HttpStatusCode.NotFound;
                _response.IsSuccess = false;
                _response.ErrorMessages = "Username or password is incorrect";
                return BadRequest(_response);
            }

            _response.StatusCode = HttpStatusCode.OK;
            _response.IsSuccess = true;
            _response.Data = loginResponse;
            return Ok(_response);
        }
        [HttpPost("register")]
        public async Task<ActionResult> Register([FromBody] RegisterRequestDto model)
        {
            ApplicationUser userFromDb = _db.ApplicationUsers
                    .FirstOrDefault(u => u.Email.ToLower() == model.Email.ToLower());

            if (userFromDb != null)
            {
                _response.StatusCode = HttpStatusCode.UnprocessableEntity;
                _response.IsSuccess = false;
                _response.ErrorMessages = "Email already exists";
                return UnprocessableEntity(_response);
            }
            ApplicationUser newUser = new()
            {
                UserName = model.Email,
                // Thay thế Username thành Email
                Email = model.Email,
                // Thay thế Username thành Email
                NormalizedEmail = model.Email.ToUpper(),
                Fulllname = model.Fullname,
            };
 
            try
            {
                var result = await _userManager.CreateAsync(newUser, model.Password);
                if (result.Succeeded)
                {
                    if (!_roleManager.RoleExistsAsync(SD.Role_Admin).GetAwaiter().GetResult())
                    {
                        // Tạo roles trong DB
                        await _roleManager.CreateAsync(new IdentityRole(SD.Role_Admin));
                        await _roleManager.CreateAsync(new IdentityRole(SD.Role_User));
                    }

                    await _userManager.AddToRoleAsync(newUser, SD.Role_User);
                    await _db.SaveChangesAsync();

                    JwtSecurityTokenHandler tokenHandler = new();
                    byte[] key = Encoding.ASCII.GetBytes(secretKey);

                    SecurityTokenDescriptor tokenDescriptor = new()
                    {
                        Subject = new ClaimsIdentity(new Claim[]
                    {
                    new Claim("fullName", newUser.Email),
                    new Claim("id",newUser.Id.ToString()),
                    new Claim(ClaimTypes.Email, newUser.Email.ToString()),
                    new Claim(ClaimTypes.Role, SD.Role_User),
                    }),
                        Expires = DateTime.UtcNow.AddDays(7),
                        SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                    };
                    SecurityToken token = tokenHandler.CreateToken(tokenDescriptor);
                    UserDto userDto = new UserDto()
                    {
                        Name = model.Fullname,
                        Email = model.Email,

                    };
                    RegisterResponseDto registerResponseDto = new()
                    {
                        //applicationUser = userFromDb.Email,
                        AccessToken = tokenHandler.WriteToken(token),
                        UserDto = userDto
                    };

                    _response.StatusCode = HttpStatusCode.OK;
                    _response.IsSuccess = true;
                    _response.Data = registerResponseDto;
                    return Ok(_response);
                }
            }
            catch (Exception e)
            {
                _response.StatusCode = HttpStatusCode.BadRequest;
                _response.IsSuccess = false;
                _response.ErrorMessages = e.Message;
                return BadRequest(_response);
            }
            _response.StatusCode = HttpStatusCode.BadRequest;
            _response.IsSuccess = false;
            _response.ErrorMessages = "Error while registering";
            return BadRequest(_response);
        }
        [HttpPost("logout")]
        [Authorize]
        public async Task<IActionResult> Logout()
        {
            _response.IsSuccess = true;
            _response.StatusCode = HttpStatusCode.OK;
            _response.Data = null;
            return Ok(_response);
        }
    }
     
}
