﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.Linq;
using WebApplication1.Data;
using WebApplication1.Dto;
using WebApplication1.Model;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class TypeController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private APIResponse _response;
        public TypeController(ApplicationDbContext db)
        {
            _db = db;
            _response = new APIResponse();
        }

        [HttpGet("get-type")]
        [Authorize]
        public async Task<ActionResult> Get()
        {
            var typeData = _db.Types.ToList();
            return Ok(typeData);
        }

        [HttpPost("post-type")]
        [Authorize]
        public async Task<ActionResult> Post([FromBody] TypeRequestDTO typeRequestDTO)
        {
            TypeImage typeImage = _db.Types.FirstOrDefault(t => t.TypeName == typeRequestDTO.TypeName);

            if (typeImage != null) {
                return BadRequest("Đã có dữ liệu trong DB");
            }

            if (typeRequestDTO.TypeName == null)
            {
                return BadRequest("Dữ liệu không hợp lệ");
            }

            TypeImage type = new TypeImage()
            {
                TypeName = typeRequestDTO.TypeName,
                CreateDate = DateTime.Now,
                UpdateAt = DateTime.Now,
            };

            await _db.Types.AddAsync(type);
            await _db.SaveChangesAsync();

            return Ok(type);
        }

        [HttpPut("put-type/{id}")]
        [Authorize]
        public async Task<ActionResult> Put([FromBody] TypeRequestDTO typeRequestDTO, long id)
        {
            TypeImage typeImageId = _db.Types.FirstOrDefault(t => t.TypeId == id);
            TypeImage typeImageName = _db.Types.FirstOrDefault(n => n.TypeName == typeRequestDTO.TypeName);

            if (typeImageId == null)
            {
                return BadRequest("Không có dữ liệu trong db");
            }

            if (typeRequestDTO.TypeName == null)
            {
                return BadRequest("Dữ liệu không hợp lệ");
            }

            if (typeImageName != null)
            {
                return BadRequest("Đã có dữ liệu trong DB");
            }

            typeImageId.TypeName = typeRequestDTO.TypeName;
            typeImageId.CreateDate = DateTime.Now;

            _db.Types.Update(typeImageId);
            await _db.SaveChangesAsync();

            return Ok(typeImageId);

        }

        [HttpDelete("delete-type/{id}")]
        [Authorize]
        public async Task<ActionResult> Delete(long id)
        {
            TypeImage typeImage = _db.Types.FirstOrDefault(t => t.TypeId == id);

            if (typeImage == null)
            {
                return BadRequest("Không có dữ liệu trong db");
            }

            _db.Types.Remove(typeImage);
            await _db.SaveChangesAsync();
            return Ok($"Xóa type với id = {id} thành công");
        }

    }
}
