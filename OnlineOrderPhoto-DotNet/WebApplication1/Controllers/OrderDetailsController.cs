﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Hosting;
using WebApplication1.Data;
using WebApplication1.Dto;
using WebApplication1.Model;
using WebApplication1.service;

using static System.Runtime.InteropServices.JavaScript.JSType;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OrderDetailsController : ControllerBase
    {
       
        private readonly ApplicationDbContext _context;
        private readonly IWebHostEnvironment _environment;
        private readonly IShoppingCardService _shoppingCardService;
       

        public OrderDetailsController(ApplicationDbContext context, 
            IWebHostEnvironment environment,
            IShoppingCardService shoppingCardService
            )
        {
            _context = context;
            _environment = environment;
            _shoppingCardService = shoppingCardService;
           
        }

        // GET: api/OrderDetails
        [HttpGet]
        [Authorize]
        public async Task<ActionResult<IEnumerable<ShoppingCardItem>>> GetOrderDetails()
        {
            var orderDetails = await _context.OrderDetails.ToListAsync();
           
            List<ShoppingCardItem> items = new List<ShoppingCardItem>();
            foreach (var orderDetail in  orderDetails)
            {
                var Size = await _context.Sizes.FindAsync(orderDetail.SizeId);
                var Type = await _context.Types.FindAsync(orderDetail.TypeId);
                var cardItem = new ShoppingCardItem();
                cardItem.Quantity = orderDetail.Quantity;
                cardItem.Price = orderDetail.Price;
                cardItem.Size = Size.PicSize;
                cardItem.Type = Type.TypeName;
                cardItem.PictureName = orderDetail.PictureName;
                cardItem.Status = orderDetail.Status;
                cardItem.OrderDetailId = orderDetail.OrderDetailId;
                items.Add(cardItem);
            }
            return items;
        }

        // GET: api/OrderDetails/5
        [HttpGet("{id}")]
        [Authorize]
        public async Task<ActionResult<OrderDetail>> GetOrderDetail(long id)
        {
            var orderDetail = await _context.OrderDetails.FindAsync(id);

            if (orderDetail == null)
            {
                return NotFound();
            }

            return orderDetail;
        }

        // PUT: api/OrderDetails/5
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPut("{id}")]
        [Authorize]
        public async Task<ActionResult> ChangeStatus(long id)
        {
            OrderDetail orderDetail = new OrderDetail();
            if(id == null)
            {
                return BadRequest();
            }
            orderDetail = await _context.OrderDetails.FindAsync(id);
            string status = orderDetail.Status;
            if(orderDetail == null)
            {
                return BadRequest();
            }
           if(status == "ORDERD")
            {


                string imageName = orderDetail.PictureName;
               var orderDetails = await _context.OrderDetails.Where(od => od.Status == "ORDERD" && od.PictureName == imageName).ToListAsync();
                if(orderDetails.Count == 1) {                
                    var filePath = Path.Combine(_environment.WebRootPath, "images", imageName);
                    Console.Write(filePath);
                    if (System.IO.File.Exists(filePath))
                    {
                        System.IO.File.Delete(filePath);

                    }
                    else
                    {
                        Console.WriteLine("Khon tim thay file anh");
                    }
                    orderDetail.Status = "PRINTED";
                }
                orderDetail.Status = "PRINTED";
            }
            _context.OrderDetails.Update(orderDetail);
            await _context.SaveChangesAsync();
            return Ok(orderDetail);
        }


        // POST: api/OrderDetails
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        [HttpPost]
        [Consumes("multipart/form-data")]
        [Authorize]
        public async Task<IActionResult> AddOrderDetails([FromForm] AddOrderDetail orderDetails)
         {
            Console.WriteLine(orderDetails.ToString());
            Console.WriteLine(orderDetails.PictureName);
            // Lấy JWT Token từ header request
            string token = HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
            var sizeAndType = orderDetails.sizeAndType;
            
            try
            {
                
                    // Kiểm tra tính hợp lệ của dữ liệu được gửi lên
                    if (!ModelState.IsValid)
                    {
                        return BadRequest(ModelState);
                    }               
                    TypeImage typeImage =  _context.Types.Where(t=>t.TypeName == sizeAndType.Type).First();
                    SizeImage sizeImage =  _context.Sizes.Where(s =>s.PicSize == sizeAndType.Size).First();
                
                OrderDetail newOrderDetail = new OrderDetail
                {
                    SizeId = sizeImage.SizeId,
                    TypeId = typeImage.TypeId,
                    PictureName = orderDetails.PictureName.FileName,
                    Quantity = sizeAndType.Quantity,
                    Price = sizeImage.SizePrice * sizeAndType.Quantity,
                    Status = "PENDING"
                    };
                    if (orderDetails.PictureName != null)
                    {
                        var originalFileName = Path.GetFileNameWithoutExtension(orderDetails.PictureName.FileName);
                        var extension = Path.GetExtension(orderDetails.PictureName.FileName);
                        var imageName = originalFileName + extension;
                   
                        string webRootPath = _environment.WebRootPath;
                        string imagePath = Path.Combine(webRootPath, "images", orderDetails.PictureName.FileName); // kết hợp đường dẫn tới thư mục images và tên của tệp tin
                        if (!Directory.Exists(imagePath)) // kiểm tra xem tệp tin đã tồn tại hay chưa
                        {
                            string fileName = Guid.NewGuid().ToString() + Path.GetExtension(orderDetails.PictureName.FileName);
                            string filePath = Path.Combine(Directory.GetCurrentDirectory(), "wwwroot/images", imageName);
                            using (var stream = new FileStream(filePath, FileMode.Create))
                            {
                                await orderDetails.PictureName.CopyToAsync(stream);
                            }
                        }
                        newOrderDetail.CreateDate= DateTime.Now;              
                            //Add vào shopping Card
                           await _shoppingCardService.AddItemAsync(newOrderDetail,token);                                                               
                    }
                    else
                    {
                        return NotFound("File ảnh không tồn tại");
                    }
                   
                
                return Ok("Thanh Cong");
            }
            catch (Exception ex)
            {
                // Xử lý lỗi nếu có
                return StatusCode(StatusCodes.Status500InternalServerError, ex.Message);
            }
        }


        // DELETE: api/OrderDetails/5
        [HttpDelete("{id}")]
        [Authorize]
        public async Task<IActionResult> DeleteOrderDetail(long id)
        {
            var orderDetail = await _context.OrderDetails.FindAsync(id);
            if (orderDetail == null)
            {
                return NotFound();
            }

            _context.OrderDetails.Remove(orderDetail);
            await _context.SaveChangesAsync();

            return NoContent();
        }

        private bool OrderDetailExists(long id)
        {
            return _context.OrderDetails.Any(e => e.OrderDetailId == id);
        }
    }
}
