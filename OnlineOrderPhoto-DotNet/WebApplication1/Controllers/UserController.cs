﻿using Azure;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;
using System.Net;
using System.Net.Http.Headers;
using System.Security.Claims;
using System.Security.Principal;
using WebApplication1.Data;
using WebApplication1.Dto;
using WebApplication1.Dto.User;
using WebApplication1.Model;
using WebApplication1.Ultility;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    public class UserController : ControllerBase
    {
        private readonly ApplicationDbContext _db;
        private APIResponse _response;
        private readonly UserManager<ApplicationUser> _userManager;
        public UserController(ApplicationDbContext db, IConfiguration configuration, UserManager<ApplicationUser> userManager)
        {
            _db = db;
            _response = new APIResponse();
            _userManager = userManager;
        }
        [HttpGet]
        [Authorize]
        public async Task<ActionResult> getUser(string userId)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                _response.StatusCode = HttpStatusCode.NotFound;
                _response.IsSuccess = false;
                _response.ErrorMessages = "Can not find user";
                return NotFound(_response);
            }
            UserDto userDto = new UserDto()
            {
                Id = user.Id,
                Name = user.Fulllname,
                Email = user.Email,
                PhoneNumber = user.PhoneNumber,
                Address = user.AddressUser,
                Dob = user.BirthDay,
                Gender = user.Gender,
            };
            _response.StatusCode = HttpStatusCode.OK;
            _response.IsSuccess = true;
            _response.ErrorMessages = "Query success";
            _response.Data = userDto;
            return Ok(_response);
        }
        [HttpGet("all")]
        [Authorize(Roles = SD.Role_Admin)]
        public async Task<ActionResult> getAllUser()
        {
            List<ApplicationUser> applicationUsers = _db.ApplicationUsers.ToList();
            if (applicationUsers.IsNullOrEmpty())
            {
                _response.StatusCode = HttpStatusCode.NotFound;
                _response.IsSuccess = false;
                _response.ErrorMessages = "Can not get all user";
                return NotFound(_response);
            }
            _response.StatusCode = HttpStatusCode.OK;
            _response.IsSuccess = true;
            _response.ErrorMessages = "Query Success";
            _response.Data = applicationUsers;
            return Ok(_response);
        }
        [HttpPut("update")]
        [Authorize]
        public async Task<ActionResult> updateUser(string userId, [FromBody] UserDto model)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                _response.StatusCode = HttpStatusCode.NotFound;
                _response.IsSuccess = false;
                _response.ErrorMessages = "Can not get user";
                return NotFound(_response);
            }
            user.PhoneNumber = model.PhoneNumber;
            user.Email = model.Email;
            user.BirthDay = model.Dob;
            user.Gender = model.Gender;
            user.Fulllname = model.Name;
            user.AddressUser = model.Address;
            await _userManager.UpdateAsync(user);
            await _db.SaveChangesAsync();
            _response.StatusCode = HttpStatusCode.OK;
            _response.IsSuccess = true;
            _response.ErrorMessages = "Update User Success";
            _response.Data = user;
            return Ok(_response);
        }
        [HttpPut("changepassword")]
        [Authorize]
        public async Task<ActionResult> changePassword(string userId, [FromBody] UpdatePasswordDto model)
        {
            ApplicationUser user = await _userManager.FindByIdAsync(userId);
            if (user == null)
            {
                _response.StatusCode = HttpStatusCode.NotFound;
                _response.IsSuccess = false;
                _response.ErrorMessages = "Can not get user";
                return NotFound(_response);
            }
            var result = await _userManager.ChangePasswordAsync(user, model.Password, model.NewPassword);
            if (result.Succeeded)
            {
                _response.StatusCode = HttpStatusCode.OK;
                _response.IsSuccess = true;
                _response.ErrorMessages = "Change Password User Success";
                _response.Data = model.NewPassword;
                return Ok(_response);
            }
            _response.StatusCode = HttpStatusCode.BadRequest;
            _response.IsSuccess = false;
            _response.ErrorMessages = "Can not change password user";
            return BadRequest(_response);
        }
    }
}
