﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebApplication1.Data;
using WebApplication1.Dto;
using WebApplication1.Model;
using WebApplication1.service;

namespace WebApplication1.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class ShoppingCardsController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly IShoppingCardService _shoppingCardService;

        public ShoppingCardsController(ApplicationDbContext context, IShoppingCardService shoppingCardService)
        {
            _context = context;
            _shoppingCardService = shoppingCardService;
        }

        // GET: api/ShoppingCards
        //Lấy danh sách shoppingCard
        [HttpGet]
        [Authorize]
        public async Task<List<ShoppingCardItem>> getShoppingCard()
        {

            // Lấy JWT Token từ header request
            string token = HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer ", "");

            return await _shoppingCardService.GetItemsAsync(token);
        }

        // POST: api/ShoppingCards
        // To protect from overposting attacks, see https://go.microsoft.com/fwlink/?linkid=2123754
        //Thêm sản phẩm vào giỏ hàng?
        [HttpPost]
        [Authorize]
        public async Task<ActionResult<ShoppingCard>> AddItemAsync(OrderDetail orderDetail,String token)
        {
            return await _shoppingCardService.AddItemAsync(orderDetail,token);
        }

        [HttpPost("Pay")]
        [Authorize]
        public async Task<ActionResult> PayInShoppingCard(Pay pay, IShoppingCardService _shoppingCardService)
        {
            // Lấy JWT Token từ header request
            string token = HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
            string result = await _shoppingCardService.PayInShoppingCard(pay, token);
            if (result == "Thanh toan thanh cong")
            {
                return Ok(result);
            }
            else
            {
                return Ok(result);
            }


        }
        // DELETE: api/ShoppingCards/5
        [HttpDelete("{orderDetailId}")]
        public Task RemoveItemAsync(long orderDetailId)
        {
            // Lấy JWT Token từ header request
            string token = HttpContext.Request.Headers["Authorization"].ToString().Replace("Bearer ", "");
            return _shoppingCardService.RemoveItemAsync(orderDetailId, token);
        }
        

    }
}
