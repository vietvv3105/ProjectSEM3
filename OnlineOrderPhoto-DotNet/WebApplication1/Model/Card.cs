﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Model;

public partial class Card : BaseEntity
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
    public int CardId { get; set; }

    public string CardName { get; set; }

    public string CardNumber { get; set; }
    [ForeignKey("UserId")]
    public string UserId { get; set; }

    //public virtual ICollection<Customer> Customers { get; } = new List<Customer>();
}
