﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Model;

public partial class Order : BaseEntity
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

    public long OrderId { get; set; }
    public string PickupName { get; set; }
    public string PickupPhone { get; set; }
    public string PickupEmail { get; set; }
    public string PickupAddress { get; set; }
    [ForeignKey("UserId")]
    public string UserId { get; set; }
    public ApplicationUser ApplicationUser { get; set; }
    public DateTime OrderDate { get; set; }
    public double TotalPrice { get; set; }

    public virtual ICollection<OrderDetail> OrderDetails { get; } = new List<OrderDetail>();
}
