﻿using System.ComponentModel.DataAnnotations;


namespace WebApplication1.Model
{
    public class ShoppingCard:BaseEntity
    {
        [Key]
        public long shopcardId { get; set; }
        public string userId { get; set; }
        public long orderdetailId { get; set; }

        public override bool Equals(object obj)
        {
            if (obj == null || GetType() != obj.GetType())
            {
                return false;
            }

            ShoppingCard other = (ShoppingCard)obj;

            return orderdetailId == other.orderdetailId
                && userId == other.userId;
                
        }


    }
}
