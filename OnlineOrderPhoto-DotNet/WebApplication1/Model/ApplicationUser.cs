﻿using Microsoft.AspNetCore.Identity;

namespace WebApplication1.Model
{
    public class ApplicationUser : IdentityUser 
    {
        //có sẵn Email, Phone
        public string Fulllname { get; set; }
        public DateTime BirthDay { get; set; }
        public int Gender { get; set; }
        public string AddressUser { get; set; }
        public Card Card { get; set; }

        public virtual ICollection<Order> Orders { get; } = new List<Order>();
    }
}
