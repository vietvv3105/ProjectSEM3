﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Model;

public partial class TypeImage : BaseEntity

{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

    public long TypeId { get; set; }

    public string TypeName { get; set; }

    public virtual ICollection<OrderDetail> OrderDetails { get; } = new List<OrderDetail>();
}
