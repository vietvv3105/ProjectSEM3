﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Model;

public partial class OrderDetail : BaseEntity
{
    [Key]
    [DatabaseGenerated(DatabaseGeneratedOption.Identity)]

    public long OrderDetailId { get; set; }
    [ForeignKey("OrderId")]
    public long? OrderId { get; set; }
    [ForeignKey("SizeId")]
    public long SizeId { get; set; }
    [ForeignKey("TypeId")]
    public long TypeId { get; set; }

    public string PictureName { get; set; }

    public int? Quantity { get; set; }

    public double Price { get; set; }

    public string Status {get; set; }
    public override bool Equals(object obj)
    {
        if (obj == null || !(obj is OrderDetail))
            return false;

        var other = (OrderDetail)obj;

        return PictureName == other.PictureName
            && SizeId == other.SizeId
            && TypeId == other.TypeId;
    }

    public override int GetHashCode()
    {
        return (PictureName.GetHashCode() ^ SizeId.GetHashCode() ^ TypeId.GetHashCode()).GetHashCode();
    }

}
