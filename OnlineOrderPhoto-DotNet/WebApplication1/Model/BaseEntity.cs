﻿namespace WebApplication1.Model
{
    public abstract class BaseEntity
    {
        
        public DateTime? CreateDate { get; set; }
        public DateTime? UpdateAt { get; set; }
    }
}
