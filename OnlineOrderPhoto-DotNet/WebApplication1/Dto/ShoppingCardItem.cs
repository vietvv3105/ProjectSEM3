﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Dto
{
    public class ShoppingCardItem
    {
        public long? OrderDetailId { get; set; }
        public string Size { get; set; }
     
        public string Type { get; set; }

        public string PictureName { get; set; }

        public int? Quantity { get; set; }

        public double Price { get; set; }

        public string Status { get; set;}
    }
}
