﻿using System.Net;

namespace WebApplication1.Dto
{
    public class APIResponse
    {
        public HttpStatusCode StatusCode { get; set; }
        public string ErrorMessages { get; set; }
        public bool IsSuccess { get; set; }
        public object Data { get; set; }
    }
}
