﻿using WebApplication1.Model;

namespace WebApplication1.Dto.User
{
    public class LoginResponseDto
    {
        public string AccessToken { get; set; }
        public UserDto userDto { get; set; }
    }
}
