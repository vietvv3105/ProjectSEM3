﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Dto.User
{
    public class RegisterRequestDto
    {
        [Required]
        public string Fullname { get; set; }
        [Required]
        public string Email { get; set; }
        [Required]
        public string Password { get; set; }
        //public string Role { get; set; }
    }
}
