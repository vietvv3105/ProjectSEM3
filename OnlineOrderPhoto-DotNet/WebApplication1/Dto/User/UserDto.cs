﻿namespace WebApplication1.Dto.User
{
    public class UserDto
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public int Gender { get; set; }
        public DateTime Dob { get; set; }
        public string Email { get; set; }
        public string Address { get; set; }
        public string PhoneNumber { get; set; }

    }
}
