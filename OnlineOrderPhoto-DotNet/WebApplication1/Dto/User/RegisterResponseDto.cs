﻿using WebApplication1.Model;

namespace WebApplication1.Dto.User
{
    public class RegisterResponseDto
    {
        public string AccessToken { get; set; }
        public UserDto UserDto { get; set; }
    }
}
