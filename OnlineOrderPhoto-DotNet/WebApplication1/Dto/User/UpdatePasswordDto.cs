﻿using System.ComponentModel.DataAnnotations;

namespace WebApplication1.Dto.User
{
    public class UpdatePasswordDto
    {
        [Required]
        public string Password { get; set; }
        [Required]
        public string NewPassword { get; set; }
    }
}
