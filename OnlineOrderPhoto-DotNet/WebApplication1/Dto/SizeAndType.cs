﻿namespace WebApplication1.Dto
{
    public class SizeAndType
    {
        public string Size { get; set; }

        public string Type { get; set; }

        public int Quantity { get; set; }
    }
}
