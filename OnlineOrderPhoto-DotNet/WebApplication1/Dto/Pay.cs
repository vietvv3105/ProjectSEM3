﻿namespace WebApplication1.Dto
{
    public class Pay
    {
        public string phone { get; set; }
        public string email { get; set; }
        public string address { get; set; }
        public string cardNumber { get; set; }
    }
}
