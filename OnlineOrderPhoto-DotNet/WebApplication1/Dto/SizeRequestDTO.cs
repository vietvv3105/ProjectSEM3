﻿namespace WebApplication1.Dto
{
    public class SizeRequestDTO
    {
        public string PicSize { get; set; }

        public double SizePrice { get; set; }

    }
}
