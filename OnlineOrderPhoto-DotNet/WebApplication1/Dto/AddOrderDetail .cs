﻿using System.ComponentModel.DataAnnotations.Schema;

namespace WebApplication1.Dto
{
    public class AddOrderDetail
    { 
        public SizeAndType sizeAndType { get; set; }
        public IFormFile PictureName { get; set; }

    }
}
