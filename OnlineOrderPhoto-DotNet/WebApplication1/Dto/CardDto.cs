﻿using Azure.Storage.Blobs.Models;

namespace WebApplication1.Dto
{
    public class CardDto
    {
        public string CardName { get; set; }
        public string CardNumber { get; set; }
    }
}
