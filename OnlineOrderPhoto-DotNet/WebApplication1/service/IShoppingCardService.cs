﻿using Microsoft.AspNetCore.Mvc;
using WebApplication1.Dto;
using WebApplication1.Model;

namespace WebApplication1.service
{
    public interface IShoppingCardService
    {   //tổng số lượng sản phẩm trong giỏ hàng.
        Task<int> GetTotalItemsAsync(String token);
        //thêm một sản phẩm vào giỏ hàng.
        Task<ActionResult<ShoppingCard>> AddItemAsync(OrderDetail orderDetail,String token);
        //xóa một sản phẩm khỏi giỏ hàng dựa trên id của sản phẩm.
        Task RemoveItemAsync(long orderDetailId, String token);

        //trả về danh sách các sản phẩm trong giỏ hàng
        Task<List<ShoppingCardItem>> GetItemsAsync(String token);

        //Thanh toán giỏ hàng
        Task<String> PayInShoppingCard(Pay pay, String token);
    }
}
