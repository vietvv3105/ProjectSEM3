﻿
using Microsoft.AspNetCore.Http.HttpResults;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.IdentityModel.Tokens.Jwt;

using System.Text;
using WebApplication1.Data;
using WebApplication1.Dto;
using WebApplication1.Model;


namespace WebApplication1.service.impl
{
    public class ShoppingCardServiceImpl : IShoppingCardService
    {
        private readonly ApplicationDbContext _context;
        private readonly IServiceProvider _serviceProvider;
      

        public ShoppingCardServiceImpl(ApplicationDbContext context, 
            IServiceProvider serviceProvider
            )
        {
            _context = context;
            _serviceProvider = serviceProvider;
         
        }
        public async Task<ActionResult<ShoppingCard>> AddItemAsync(OrderDetail newOrderDetail,String token)
        {
            var userId = getUserId(token);        
            List<ShoppingCard> items = await _context.ShoppingCards.Where(i=>i.userId.Equals(userId)).ToListAsync();
            List<OrderDetail> orderDetailList = new List<OrderDetail>();
            foreach(var item in items)
            {
                var orderDetailId = item.orderdetailId;
                OrderDetail orderDetail = _context.OrderDetails.Where(od=>od.OrderDetailId == orderDetailId).FirstOrDefault();
                orderDetailList.Add(orderDetail);
            }
            ShoppingCard newShoppingCard = new ShoppingCard();
            Console.WriteLine(orderDetailList.Count);
            //Check Order nếu tồn tại + thêm Quantity
            var exist = false;
     
           
            if(orderDetailList.Count >0)
            {
                foreach (OrderDetail o in orderDetailList)
                {
                    if (newOrderDetail.Equals(o))
                    {
                        o.Quantity += newOrderDetail.Quantity;
                        _context.Entry(o).State = EntityState.Modified;
                        await _context.SaveChangesAsync();
                        exist = true;
                    }

                }
                if (!exist)
                {
                    _context.OrderDetails.Add(newOrderDetail);
                    await _context.SaveChangesAsync();                 
                    newShoppingCard.userId = userId;
                    newShoppingCard.orderdetailId = newOrderDetail.OrderDetailId;
                    _context.ShoppingCards.Add(newShoppingCard);
                    await _context.SaveChangesAsync();
                    return (newShoppingCard);
                }
            }
            else if (orderDetailList.Count ==0 )
            {
                _context.OrderDetails.Add(newOrderDetail);
                await _context.SaveChangesAsync();
                newShoppingCard.userId = userId;
                newShoppingCard.orderdetailId = newOrderDetail.OrderDetailId;
                _context.ShoppingCards.Add(newShoppingCard);
                await _context.SaveChangesAsync();

                return (newShoppingCard);
            }
           
            return (newShoppingCard);
            
        
        }
        private string getUserId(String token)
        {

            // Xác thực JWT Token
            var tokenHandler = new JwtSecurityTokenHandler();
            var key = Encoding.ASCII.GetBytes("Online Order of Digital Photo Printing");
            var validationParameters = new TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false
            };
            SecurityToken validatedToken;
            var claimsPrincipal = tokenHandler.ValidateToken(token, validationParameters, out validatedToken);

            // Giải mã thông tin user từ JWT Token
            var jwtToken = (JwtSecurityToken)validatedToken;
            var userId = jwtToken.Claims.First(x => x.Type == "id").Value;
            Console.WriteLine(userId);
            return userId;
        }
        private ActionResult<ShoppingCard> NotFound(string v)
        {
            throw new NotImplementedException();
        }

        public async Task<List<ShoppingCardItem>> GetItemsAsync(String token)
        {
            string userId = getUserId(token);
            var shoppingCards = await _context.ShoppingCards.Where(s => s.userId == userId).ToListAsync();
            List<OrderDetail> orderDetails = new List<OrderDetail>();
            foreach (var shopCard in shoppingCards)
            {
                var orderDetailId = shopCard.orderdetailId;
                var orderDetail = await _context.OrderDetails.FindAsync(orderDetailId);
         
                orderDetails.Add(orderDetail);
            }
            List<ShoppingCardItem> items = new List<ShoppingCardItem>();
            foreach(var orderDetail in orderDetails)
            {
                var Size = await _context.Sizes.FindAsync(orderDetail.SizeId);
                var Type = await _context.Types.FindAsync(orderDetail.TypeId);
                var cardItem = new ShoppingCardItem();
                cardItem.Quantity = orderDetail.Quantity;
                cardItem.Price = orderDetail.Price;
                cardItem.Size = Size.PicSize;
                cardItem.Type = Type.TypeName;
                cardItem.PictureName = orderDetail.PictureName;           
                cardItem.Status = orderDetail.Status;
                items.Add(cardItem);
            }
            return items;
        }

        public Task<int> GetTotalItemsAsync(String token)
        {
            throw new NotImplementedException();
        }

        public async Task<ShoppingCard> GetShoppingCardByOrderDetailId(long orderDetailId,String token)
        {
            var userId = getUserId(token);
            var shoppingcard = await _context.ShoppingCards.FirstOrDefaultAsync(s => s.userId == userId && s.orderdetailId == orderDetailId);
            return shoppingcard;
        }

        public async Task RemoveItemAsync(long orderDetailId,String token)
        {
            ShoppingCard shoppingCard = await GetShoppingCardByOrderDetailId(orderDetailId,token);
            _context.ShoppingCards.Remove(shoppingCard);
            await _context.SaveChangesAsync();
        }
        //Thanh toans
        public async Task<String> PayInShoppingCard(Pay pay,String token)
        {
            var userId = getUserId(token);
            Order order = new Order();
            Card newCard = new Card();
            order.UserId = userId;
            order.OrderDate = DateTime.Now;
            order.PickupAddress = pay.address;
            order.PickupPhone = pay.phone;
            order.PickupEmail = pay.email;
            List<ShoppingCard> shoppingCardByUserId = await _context.ShoppingCards.Where(s => s.userId == userId).ToListAsync();
            List<OrderDetail> orderDetails = new List<OrderDetail>();
            if (pay.cardNumber != null && pay.GetType != null)
            {
                if (validateCardNumber(pay.cardNumber))
                {
                    newCard.CardName = "Credit Card";
                    newCard.CardNumber = pay.cardNumber;
                }
                else
                {
                    return "The khong hop le";
                }

                _context.Cards.Add(newCard);
                await _context.SaveChangesAsync();
            }
            foreach (ShoppingCard sh in shoppingCardByUserId)
            {
                var orderDetailId = sh.orderdetailId;
                var orderDetail = await _context.OrderDetails.FindAsync(orderDetailId);
                orderDetails.Add(orderDetail);
            }
            Console.WriteLine(orderDetails);
            double totalPrice = 0;
            foreach (var orderDetail in orderDetails)
            {
                totalPrice += orderDetail.Price;
           
            }
            order.TotalPrice = totalPrice;
            order.CreateDate = DateTime.Now;
            _context.Orders.Add(order);
            await _context.SaveChangesAsync();
            foreach(var orderDetail in orderDetails)
            {
                orderDetail.OrderId = order.OrderId;          
                orderDetail.Status = "ORDERD";
                _context.Entry(orderDetail).State = EntityState.Modified;
                await _context.SaveChangesAsync();
                await RemoveItemAsync(orderDetail.OrderDetailId,token);
            }
           
            return "Thanh toan thanh cong";
        }
        static bool validateCardNumber(string input)
        {
            //convert input to int
            int[] cardNumber = new int[input.Length];

            for (int i = 0; i < input.Length; i++)
            {
                cardNumber[i] = (int)(input[i] - '0');
            }

            //Starting from the right, double each other digit, if greater than 9, mod 10 and + 1 to reminder
            for (int i = cardNumber.Length - 2; i >= 0; i = i - 2)
            {
                int tempValue = cardNumber[i];
                tempValue = tempValue * 2;
                if (tempValue > 9)
                {
                    tempValue = tempValue % 10 + 1;
                }
                cardNumber[i] = tempValue;
            }
            //Add up all digit
            int total = 0;
            for (int i = 0; i < cardNumber.Length; i++)
            {
                total += cardNumber[i];
            }

            //if number is multiple of 10, its valid
            if (total % 10 == 0)
            {
                return true;

            }
            else
            {
                return false;
            }

        }


    }
}
